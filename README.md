Projet React Native
Thème : Application mobile de type Todo List

Fonctionnalités de l'application :
    - Permet à l'utilisateur de créer une liste de tâches
    - Permet à l'utilisateur d'utiliser une couleur pour chaque liste
    - Permet à l'utilisateur d'ajouter des tâche à ses listes
    - Permet à l'utilisateur de marquer si une tâche est terminée ou pas

Perspectives de l'application :
    - Permettre à l'utilisateur de s'identifier
    - Ajouter la possibilité de stocker ses données en ligne avec firebase de sorte à ce que        l'utilisateur puisse avoir accès à ces données depuis n'importe quel appareil

Membres du groupe :
    - DIARRA Kodana Adama
    - FOFANA Omar Ahmed
